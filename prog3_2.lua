function InfixToPostfix(str)

strtok = {}
stack = {} 
weight = {}
weight["^"] = 3
weight["*"] = 2 
weight["/"] = 2
weight["-"] = 1
weight["+"] = 1
output = ""

print("Assignment #3, Mark Park, mark619park@gmail.com")
for token in string.gmatch(str, "[^%s]+") do     --tokenizes input by space delimiters and stores it into the x table. 
    table.insert(strtok,token)
    print(token)  
    end
    
     
    for i = 1, #strtok do  --Until ya reach the max length of strtok 
        if strtok[i] == "+" or strtok[i] == "-" or strtok[i] == "*" or strtok[i] == "/" or strtok[i] == "^" then 
            while #stack ~= 0 and weight[stack[#stack]] >= weight[strtok[i]] do     --#stack instead of using count because it holds the max length 
            output = output .. " " .. stack[#stack]                         
            table.remove(stack,#stack)            
           
            end 
            table.insert(stack,strtok[i])
        else 
           
            output = output .. " " .. strtok[i] --Since we checked if it is an operator already, we can assume if it got to this point, it is an operand. 
        end
    end
    
    while #stack ~= 0 do                        
        output = output .. " " .. stack[#stack] --Pop the stack and attach whatever was popped to the output string
        table.remove(stack,#stack)              
        
    end
    return output 
end
      