#include <lua.hpp>
#include <iostream> 
using namespace std;



int main(int argc, char *argv[]){ 
    string line = "";
    cout << "Assignment #3-3, Mark Park, mark619park@gmail.com" << endl;
    lua_State *L = luaL_newstate(); 
    luaL_openlibs(L);
    luaL_dofile(L, argv[1]);         //File input
    lua_getglobal(L, "InfixToPostfix"); //tells stack that i am going to call function 
    getline(cin, line);            //Gets the standards into string 
    lua_pushstring(L, line.c_str()); //converts string into an array of characters.     
    lua_pcall(L, 1, 1, 0);             // From Left to Right, Number of Args, Number of Results, Number of Errors. 
    line = luaL_checkstring(L,1);    //grabs returned value from function 
    cout << line << endl;          
    lua_close(L);                    //Close the Lua Environment  
        
    return 0;
}

    