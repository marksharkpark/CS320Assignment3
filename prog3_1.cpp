#include <lua.hpp>
#include <iostream> 
using namespace std;

int main(int argc, char *argv[]){ 
    lua_State *L = luaL_newstate(); 
    luaL_openlibs(L);
    luaL_dofile(L, argv[1]);
    lua_close(L);
    cout << "Mark Park, Assignment #3-1, mark619park@gmail.com";
    return 0;
}

    